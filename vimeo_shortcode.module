<?php
/**
 * @file
 * Adds a theme function and input filter to assist in embedding Vimeo players into textareas.
 */
 
function vimeo_shortcode_menu() {
     $items['admin/config/media/vimeo-shortcode'] = array(
    'title' => 'Vimeo shortcode',
    'description' => 'Adjust vimeo shortcode settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('vimeo_shortcode_settings'),
    'access arguments' => array('change vimeo shortcode settings'),
  );
  return $items; 
} 

/**
 * Implements hook_permission().
 */
function vimeo_shortcode_permission() {
  return array(
    'change vimeo shortcode settings' => array(
      'title' => t('Change vimeo shortcode setting'),
      'description' => t('Change settings such as default width and height, and restrict the kinds of videos that can be shown.'),
    ),
  );
}    
 

/**
 * Implements hook_theme().
 */
function vimeo_shortcode_theme() {
  return array(
    'vimeo_shortcode' => array(
      'variables' => array(
        'options' => array(),
        'classes' => '',
        'caption' => '',
        'embed_code' => ''
      ),
      'template' => 'theme/vimeo-embedded-player',
    ),
    'vimeo_shortcode_multiple' => array(
      'variables' => array('options' => array()),
      'template' => 'theme/vimeo-multiple',
    )
  );
}


/**
 * Implements hook_libraries_info().
 */
function vimeo_shortcode_libraries_info() {
  $libraries = array();
  $libraries['fluidvids'] = array(
    'name' => 'FluidVids',
    'vendor url' => 'http://toddmotto.com/labs/fluidvids',
    'download url' => 'https://github.com/toddmotto/fluidvids/archive/master.zip',
    'version callback' => 'short_circuit_version',
    'files' => array(
      'js' => array(
        'dist/fluidvids.min.js',
      ),
    ),
  );
  $libraries['vimeowrap'] = array(
    'name' => 'vimeowrap.js',
    'vendor url' => 'http://luwes.co/labs/vimeo-wrap',
    'download url' => 'https://github.com/luwes/vimeowrap.js/archive/master.zip',
    'version callback' => 'short_circuit_version',
    'files' => array(
      'js' => array(
        'vimeowrap.js',
      ),
    ),
  );  
  return $libraries;  
}


/**
* Short-circuit the version argument.
* from http://atendesigngroup.com/blog/adding-js-libraries-drupal-project-libraries-api
*/
function short_circuit_version() { 
  return TRUE;
}

/* This is not ideal - right now this is loading on every page load
   we should get it so it loads only when the videos load
   but then it doesnt appear when page is cached */
function vimeo_shortcode_init() {
  // Load js libraries if there
  if(module_exists('libraries') && $library = libraries_load('fluidvids')) {
    if($library['loaded']) {
      drupal_add_js("Fluidvids.init({ selector: 'iframe', players: ['player.vimeo.com']});", array('type' => 'inline', 'scope' => 'footer'));
    }
  }
}

/**
 * Implements hook_preprocess_theme().
 */
function vimeo_shortcode_preprocess_vimeo_shortcode(&$variables) {

  // Add classes to outer div of template
  if (isset($variables['options']['class'])) {
    $classes = explode(' ', $variables['options']['class']);
    foreach($classes as $class) {
      $variables['classes_array'][] = $class;
    }
  }
  if (isset($variables['options']['caption'])) {
    $caption = trim($variables['options']['caption']);
    $variables['caption'] = !empty($caption) ? '<div class="caption">' . $caption . '</div>' : ' ';
  }
  $video_url = $variables['options']['url'];

  $xml_url = 'http://vimeo.com/api/oembed.xml?url=' . rawurlencode($video_url);

  if(!empty($variables['options']['width'])) {
    $xml_url .= '&width=' . $variables['options']['width'];
  }
  if(!empty($variables['options']['height'])) {
    $xml_url .= '&height=' . $variables['options']['height'];
  }
  $oembed = simplexml_load_string(curl_get($xml_url));
  
  if (variable_get('vimeo_shortcode_plus', 1) && $oembed->is_plus == 0) {
    $variables['embed_code'] = t('Only Vimeo Plus videos may be embedded here');
  }  
  $restrict = variable_get('vimeo_shortcode_restrict_owners', '');
  $owners = explode("\n", $restrict);
  
  if (!empty($restrict) && !in_array($oembed->author_url, $owners)) {
    $variables['embed_code'] = t('The Vimeo account specified may not embed videos on this site.');   
  }    
  else {     
    $variables['embed_code'] = html_entity_decode($oembed->html);
  }  
}  

function vimeo_shortcode_preprocess_vimeo_shortcode_multiple(&$variables) {

  drupal_add_js(libraries_get_path('vimeowrap') . '/vimeowrap.js'); 
  if ($variables['options']['display'] == 'carousel') { 
    drupal_add_js(libraries_get_path('vimeowrap') . '/vimeowrap.carousel.js');
  } else {
    $variables['options']['display'] = 'playlist';
    drupal_add_js(libraries_get_path('vimeowrap') .  '/vimeowrap.playlist.js');
  }  

  // Need to encase URLs in quotes to use as vimeowrap args
  $urls = explode(',', $variables['options']['url']);
  $newurls = array();
  foreach ($urls as $url) {
    $url = trim($url);
    $newurls[] = "'" . $url . "'";
  } 
  $variables['options']['url'] = implode(',', $newurls); 
  
  // Add classes to outer div of template  
  if (isset($variables['options']['class'])) {
    $variables['video_classes'] = $variables['options']['class'];
  }  
  if (isset($variables['options']['caption'])) {
    $caption = trim($variables['options']['caption']);
    $variables['caption'] = !empty($caption) ? '<div class="caption">' . $caption . '</div>' : ' ';
  }  
  
  $playerid = mt_rand(0, 1000000);
  
  $variables['embed_code'] = '<div id="player-'. $playerid  .'"></div>';
  
  drupal_add_js("vimeowrap('player-" . $playerid . "').setup({\n"
        . $variables['options']['baseoptionsargs'] . 
    "urls: [" . $variables['options']['url'] . "],
    plugins: { '" . $variables['options']['display'] . "':{". $variables['options']['pluginoptionsargs'] ."} }
  });
  jQuery(window).load(function () {
    jQuery('#player-" . $playerid . "').css('width', '100%');
    var width = document.getElementById('player-" . $playerid . "').offsetWidth;
    var height = width * .6;
    var totalheight = height + 200;
    jQuery('#player-" . $playerid . "').css('height', totalheight + 'px');
    jQuery('#player-" . $playerid . "_playlist').css('width', '100%');
    jQuery('#player-" . $playerid . "_playlist_wrap').css('width', '100%');
    jQuery('#player-" . $playerid . "_display').css('width', '100%');
    jQuery('#player-" . $playerid . " iframe').attr('width', '100%');
    jQuery('#player-" . $playerid . "_display').css('height', height + 'px');
    jQuery('#player-" . $playerid . "_playlist').css('top', height + 'px');
    jQuery('#player-" . $playerid . " iframe').attr('height', height);
  });  



  ", array('type' => 'inline', 'scope' => 'footer'));

} 


function vimeo_shortcode_filter_info() {
  $filters['vimeo_shortcode'] = array(
    'title' => t('Video shortcode filter'), 
    'description' => t('Substitutes [video url=""] with embedding code.'), 
    'process callback' => '_vimeo_shortcode_filter_process_text', 
    'tips callback' => '_vimeo_shortcode_filter_tips',
  );

  return $filters;
}

/**
 * Processes text obtained from the input filter.
 * 
 * This function processes the filter text that the user has added to the text area.
 * If the filter is wrapped in <p></p> then these are stripped as part of the processing
 * This eliminates a validation error in the resulting mark up if the filter is
 * being used in conjunction with other HTML filters that correct line breaks.
 * It won't work in EVERY case, but it will work in MOST cases.
 * Filters that are embedded in-line with text will continue to fail validation.
 */
function _vimeo_shortcode_filter_process_text($text, $filter) {
 $endl = chr(13) ;
  if (preg_match_all('@(?:<p>)?\[(vimeo|video)\s*(.*?)\](?:</p>)?@s', $text, $match)) {
    
    //print_r($match[2]);
    
    // set these in settings menu
    //$allowed_parameters = array('url', 'width', 'height', 'class', 'caption', 'display');
    $match_vars = array();
    
    // Initialise an array to hold playlist arrays
    $files = array();
    $matched = array();
    $replace = array();
    $prepared = array();
    
    foreach ($match[2] as $key => $passed_parameters) { 
      //preg_match_all('/(\w*)=\"(.*?)\"/', $passed_parameters, $match_vars[$key]);
      preg_match_all('/(\w*)=(?:\"|&quot;)(.*?)(?:\"|&quot;)/', $passed_parameters, $match_vars[$key]);
      // $match_vars[0][#] .... fully matched string
      // $match_vars[1][#] .... matched parameter, eg flashvars, params
      // $match_vars[2][#] .... value after the '='
      
      // Process the parameters onto the $options array.
      // Search for standard parameters, parse the values out onto the array.
      
      //print_r($match_vars);
      
      
      foreach ($match_vars[$key][1] as $vars_key => $vars_name) {
        
        $options = array();
        //if (in_array($vars_name, $allowed_parameters)) {   
          $prepared[$key][$vars_name] = $match_vars[$key][2][$vars_key];
          unset($match_vars[$key][1][$vars_key]);
        //}
      }  
      $options = $prepared[$key];
      
      $urlargs = explode('/', substr($options['url'], 8));
      if ($urlargs[1] == 'album' || $urlargs[1] == 'channels' || strpos($options['url'], ',') !== FALSE) {
        
        $defaultargs = explode('to', variable_get('vimeo_shortcode_default_ratio', '16to9'));
        if(!empty($options['width']) && empty($options['height'])) {
            $options['height'] = (int) ($options['width'] * $defaultargs[1] / $defaultargs[0]);
        }  
        if(empty($options['width']) && !empty($options['height'])) {
            $options['width'] = $options['height'] * $defaultargs[0] / $defaultargs[1];
        }  

        $options['class'] = trim('vimeo-player ' . $options['class']); 
        
        $baseoptions = array('width', 'height', 'class', 'caption', 'color', 'item', 'repeat', 'autoplay', 'portrait', 'title', 'byline');
        $pluginoptions = array('position', 'size', 'offsetx', 'offsety', 'visible', 'autoplay', 'speed', 'thumb.quality', 'thumbwidth', 'thumbheight');
        $baseoptionsargs = '';
        $pluginoptionsargs = '';
        
        foreach ($options as $optionkey => $value) {
          if (in_array($optionkey, $baseoptions)) {
            $valuewrapped = is_numeric($value) || is_bool($value) ? $value : "'" . $value . "'"; 
            $baseoptionsargs .= $optionkey . ': ' . $valuewrapped . ",\n";
          }    
          if (in_array($optionkey, $pluginoptions)) {
            $valuewrapped = is_numeric($value) || is_bool($value) ? $value : "'" . $value . "'"; 
            $pluginoptionsargs .= $optionkey . ': ' . $valuewrapped . ',';
          }    
        } 
        $options['baseoptionsargs'] = $baseoptionsargs;
        $options['pluginoptionsargs'] = rtrim($pluginoptionsargs, ',');
        $replace = theme('vimeo_shortcode_multiple', array('options' => $options));
        
      } else { 
        $replace = theme('vimeo_shortcode', array('options' => $options));
      }  
      
      $matched[] = $match[0][$key];
      $rewrite[] = $replace;
    } 
    return str_replace($matched, $rewrite, $text);

  }
  return $text;
}

function _vimeo_shortcode_filter_tips () {
   return 'Use [vimeo:url="..." width="" height=""] to embed vimeo videos]';   
}  

// Curl helper function to retrieve data using API
function curl_get($url) {
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_TIMEOUT, 30);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
    $return = curl_exec($curl);
    curl_close($curl);
    return $return;
} 

function vimeo_shortcode_settings() {
    
  $form['vimeo'] = array(
    '#type' => 'fieldset',
    '#title' => t('Vimeo settings'),
  );  
    
  $form['vimeo']['vimeo_shortcode_plus'] = array(
    '#type' => 'checkbox',
    '#title' => t('Only embed Vimeo plus videos'),
    '#default_value' => variable_get('vimeo_shortcode_plus', 1),
    '#description' => t('Useful if you want to keep your site free of other brands.'),
  );
  $form['vimeo']['vimeo_shortcode_restrict_owners'] = array(
    '#type' => 'textarea',
    '#title' => t('Restrict videos to the following owners'),
    '#default_value' => variable_get('vimeo_shortcode_restrict_owners', ''),
    '#description' => t('Place one username per line. If no users specified, then none will be imported'),
  );
  $form['vimeo']['vimeo_shortcode_default_ratio'] = array(
    '#type' => 'radios',
    '#options' => array(
      '16to9' => '16:9',
      '4to3' => '4:3',
      '11to7' => '11:7 (good intermediate ratio if your playlist contains both 4:3 and 16:9)'
     ), 
    '#title' => t('Default ratio of videos in playlist/carousel'),
    '#default_value' => variable_get('vimeo_shortcode_default_ratio', '16to9'),
    '#description' => t('If a video width is given in a playlist and no height (or vice-versa) then the missing dimension is calculated according to this ratio. For single videos, the ratio is automatically calculated.'),
  );  
 
  return system_settings_form($form);
} 



